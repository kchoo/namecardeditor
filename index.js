
window.canvas = null;
$(function() {
  window.canvas = new fabric.Canvas('canvas', {top : "200px"});

  $("#remove-text").on("click", function() {
    var activeObject = canvas.getActiveObject();
    if (activeObject && activeObject.type === 'text') {
      activeObject.remove();
    }
  });

  $("#insert-text").on("click", function() {
    var color = "#" + $('#text-color').val();
    var font = $("#font").val();
    var fontSize = $("#font-size").val();
    var fontWeight = $("#font-weight").val();
    var text = new fabric.Text($('#text').val(), { left: 100, top: 100, fill: color,
  fontFamily: font , fontWeight: fontWeight, fontSize: fontSize});
    canvas.add(text);
  });

  $("#change-color").on("click", function() {
    var color = "#" + $("#background-color").val();
    var activeObject = canvas.getActiveObject();
    console.log(activeObject);
    if (activeObject && activeObject.type === 'rect') {
      activeObject.setFill(color);
      canvas.renderAll();
    }
  });

  // create a rectangle object
  var rect = new fabric.Rect({
    left: 0,
    top: 0,
    fill: 'white',
    hasControls: false,
    lockMovementX: true,
    lockMovementY: true,
    hasBorders: false,
    width: canvas.width,
    height: canvas.height
  });

  // "add" rectangle onto canvas
  canvas.add(rect);

  window.check = function() {
    console.log(canvas.item(0));
  }


  new UIProgressButton( $("#export-pdf")[0], {
      callback : function( instance ) {
          var progress = 0,
              interval = setInterval( function() {
                  progress = Math.min( progress + Math.random() * 0.1, 1 );
                  instance.setProgress( progress );

                  if( progress === 1 ) {
                      instance.stop( 1 );
                      clearInterval( interval );
                      var c = document.getElementById("canvas");
                      var imgData = c.toDataURL("image/jpeg", 1.0);
                      var pdf = new jsPDF('l', 'in', [3.5, 2]);

                      pdf.addImage(imgData, 'JPEG', 0, 0);
                      pdf.save("download.pdf");
                  }
              }, 150 );
      }
  });

  new UIProgressButton( $("#export-jpeg")[0], {
      callback : function( instance ) {
          var progress = 0,
              interval = setInterval( function() {
                  progress = Math.min( progress + Math.random() * 0.1, 1 );
                  instance.setProgress( progress );

                  if( progress === 1 ) {
                      instance.stop( 1 );
                      clearInterval( interval );
                      var c = document.getElementById("canvas");
                      var dt = c.toDataURL('image/jpeg');
                      window.location = dt;
                  }
              }, 150 );
      }
  });

});
